Join us on April 4th, 2022 for the inaugural Lawyers on the Links golf tournament! Presented by Market My Market, the tournament takes place just steps from the Vegas strip at the top-rated Bali Hai Golf Resort which has been named one of the Top 50 Resort Courses by Golf Digest. Bring together your colleagues and friends from the plaintiffs bar for a day of golf, great prizes, and camaraderie!

The 2022 Lawyers on the Links golf tournament is the perfect way to kick off the April MTMP. Join us at noon on April 4th for a day of golf, spectating, food, drinks, and an all-around good time!


Website: https://lawyersonthelinks.com
